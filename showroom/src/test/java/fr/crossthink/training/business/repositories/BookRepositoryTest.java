package fr.crossthink.training.business.repositories;

import fr.crossthink.training.java.Application;
import fr.crossthink.training.java.business.models.Book;
import fr.crossthink.training.java.business.models.Library;
import fr.crossthink.training.java.business.repositories.BookRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Application.class}, webEnvironment = SpringBootTest.WebEnvironment.NONE)
@TestPropertySource(locations = "classpath:application.properties")
@Transactional
public class BookRepositoryTest {

    @Autowired
    private BookRepository bookRepository;

    @Before
    public void setUp() {
        bookRepository.findAll();
    }

    @Test
    public void shoudSaveBookAndLibrary() {
        Book book = saveBookAndLibrary();

        assertThat(book.getId()).isNotNull();
    }

    @Test
    public void shoudGetBookAndLibrary() {
        Book book = saveBookAndLibrary();

        Book savedBook = bookRepository.getOne(book.getId());

        assertThat(savedBook.getLibrary()).isNotNull();
    }


    /* Pivate methods */
    private Book saveBookAndLibrary() {
        Book book = createRandomBook();
        book.setLibrary(createRandomLibrary());

        return bookRepository.save(book);
    }

    private Book createRandomBook() {
        Book book = new Book();
        book.setTitle(randomAlphabetic(10));
        book.setAuthor(randomAlphabetic(15));
        return book;
    }

    private Library createRandomLibrary() {
        Library library = new Library();
        library.setName(randomAlphabetic(10));
        library.setCity(randomAlphabetic(15));
        return library;
    }
}
