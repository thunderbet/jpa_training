package fr.crossthink.training.it.glue;

import fr.crossthink.training.java.business.models.Book;
import fr.crossthink.training.java.business.services.BookService;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.api.Assertions;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class BookStepDefs {

    @Autowired
    BookService bookService;

    Book book;

    String author;

    List<Book> bookList;

    @Before
    public void setUp()  {
        Book gosFormula = new Book("La formule de dieu", "José Rodrigues dos Santos");
        Book salomonsKey = new Book("La clé de Salomon", "José Rodrigues dos Santos");
        Book angelsAndDemons = new Book("Anges et démons", "Dan Brown");
        bookService.saveBook(gosFormula);
        bookService.saveBook(salomonsKey);
        bookService.saveBook(angelsAndDemons);
    }

    @Given("the author {string}")
    public void theAuthor(String author) {
        this.author = author;
    }

    @When("the service is queried")
    public void theServiceIsQueried() {
        bookList = bookService.findByAuthor(author);
    }

    @Then("the following books are found :")
    public void theFollowingBooksAre(List<Book> books) {
        Assertions.assertThat(bookList)
                .usingElementComparatorIgnoringFields("id", "library")
                .containsExactlyInAnyOrderElementsOf(books);
    }

    @After
    public void tearDown()  {
        bookService.deleteAll();
    }



    @Given("the following book :")
    public void theFollowingBook(Book book) {
        this.book = book;
    }

    @When("the service saves the book")
    public void theServiceSavesTheBook() {
        bookService.saveBook(book);
    }

    @Then("the book is created")
    public void theBookIsCreated() {
        book = bookService.getBookById(book.getId());
        assertThat(book)
                .withFailMessage("The book was not created")
                .isNotNull();
    }




    @When("the service deletes the book")
    public void theServiceDeletesTheBook() {
        book = bookService.findByTitleAndAuthor(book.getTitle(), book.getAuthor());
        bookService.deleteBook(book);
    }

    @Then("the book is deleted")
    public void theBookIsDeleted() {
        boolean bookExists = bookService.bookExists(book.getId());
        assertThat(bookExists)
                .withFailMessage("The book was not deleted")
                .isFalse();
    }

}
