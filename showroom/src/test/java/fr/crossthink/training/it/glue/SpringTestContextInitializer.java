package fr.crossthink.training.it.glue;


import fr.crossthink.training.java.Application;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.cucumber.java.Before;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;


@SpringBootTest(classes = {Application.class}, webEnvironment = SpringBootTest.WebEnvironment.NONE)
@TestPropertySource(locations="classpath:application.properties")
public class SpringTestContextInitializer {

    private static final Logger LOGGER = LoggerFactory.getLogger(SpringTestContextInitializer.class);

    /**
     * Ensure that the Spring Context is initialized before every test
     */
    @Before
    public void setUp() {
       LOGGER.info("Initializing Spring Context for integration tests");
    }
}
