package fr.crossthink.training.it;

import org.junit.runner.RunWith;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"classpath:features"},
        glue = {"classpath:fr.crossthink.training.it.glue"},
        tags = {"not @ignore"},
        strict = true)
public class CukeRunnerIT {
}
