Feature: Testing the book service

  Scenario: Create a book
    Given the following book :
      | title          | author    |
      | Da Vinci Code  | Dan Brown |
    When the service saves the book
    Then the book is created

  Scenario: Delete a book
    Given the following book :
      | title          | author    |
      | Anges et démons  | Dan Brown |
    When the service deletes the book
    Then the book is deleted

  Scenario: List all books of the author José Rodrigues dos Santos
    Given the author "José Rodrigues dos Santos"
    When the service is queried
    Then the following books are found :
      | title               | author                    |
      | La formule de dieu  | José Rodrigues dos Santos |
      | La clé de Salomon   | José Rodrigues dos Santos |