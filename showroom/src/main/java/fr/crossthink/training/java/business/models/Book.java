package fr.crossthink.training.java.business.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "BOOK",
        indexes = {
                @Index(
                        name = "IDX_BOOK_TITLE",
                        columnList = "TITLE",
                        unique = false
                ),
                @Index(
                        name = "IDX_BOOK_AUTHOR",
                        columnList = "AUTHOR",
                        unique = false
                ),
        }
)
public class Book {

    @Id
    @Column(name="ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "TITLE", nullable = false, unique = true)
    private String title;

    @Column(name = "AUTHOR", nullable = false)
    private String author;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    private Library library;

    public Book() {
    }

    public Book(String title, String author) {
        this.title = title;
        this.author = author;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Library getLibrary() {
        return library;
    }

    public void setLibrary(Library library) {
        this.library = library;
    }
}