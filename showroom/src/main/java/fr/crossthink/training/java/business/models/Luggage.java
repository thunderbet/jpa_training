package fr.crossthink.training.java.business.models;

import javax.persistence.*;

@Entity
@Table(name = "LUGGAGE",
        indexes = {
                @Index(
                        name = "IDX_LUGGAGE_BRAND",
                        columnList = "LUGGAGE_BRAND",
                        unique = false
                )
        })
public class Luggage {

    @Id
    @Column(name = "LUGGAGE_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "LUGGAGE_BRAND", nullable = false)
    private String luggageBrand;

    @Column(name = "LUGGAGE_WEIGHT", nullable = false)
    private Integer luggageWeight;

    @ManyToOne
    @JoinColumn(name = "PASSENGER_ID")
    private Passenger passenger;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLuggageBrand() {
        return luggageBrand;
    }

    public void setLuggageBrand(String luggageBrand) {
        this.luggageBrand = luggageBrand;
    }

    public Integer getLuggageWeight() {
        return luggageWeight;
    }

    public void setLuggageWeight(Integer luggageWeight) {
        this.luggageWeight = luggageWeight;
    }

    public Passenger getPassenger() {
        return passenger;
    }

    public void setPassenger(Passenger passenger) {
        this.passenger = passenger;
    }
}
