package fr.crossthink.training.java.business.dto;

import java.util.Objects;

public class AirportDTO {

    private long id;

    private String city;

    private String cityCode;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AirportDTO that = (AirportDTO) o;
        return city.equals(that.city) &&
                cityCode.equals(that.cityCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(city, cityCode);
    }
}
