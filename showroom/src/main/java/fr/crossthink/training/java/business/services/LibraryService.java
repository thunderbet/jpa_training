package fr.crossthink.training.java.business.services;

import fr.crossthink.training.java.business.models.Library;
import fr.crossthink.training.java.business.repositories.LibraryRepository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Library service
 */
@Service
public class LibraryService {

    private LibraryRepository libraryRepository;

    /**
     * Constructor with parameter injection
     *
     * @param libraryRepository
     */
    public LibraryService(LibraryRepository libraryRepository) {
        this.libraryRepository = libraryRepository;
    }

    /**
     * Saves a library
     *
     * @param library
     * @return
     */
    public Library saveLibrary(Library library) {
        return libraryRepository.save(library);
    }

    /**
     * Gets a library with the given id
     *
     * @param id
     * @return
     */
    public Library getLibrary(Long id) {
        return libraryRepository.getOne(id);
    }

    /**
     * Gets all libraries
     *
     * @return
     */
    public List<Library> getAllLibraries() {
        return libraryRepository.findAll();
    }

    /**
     * Deletes all libraries, for test clean up
     */
    public void deleteAll() { libraryRepository.deleteAll(); }
}
