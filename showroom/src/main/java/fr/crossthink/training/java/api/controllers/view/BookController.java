package fr.crossthink.training.java.api.controllers.view;

import fr.crossthink.training.java.api.vm.BookCreateVM;
import fr.crossthink.training.java.business.dto.BookDTO;
import fr.crossthink.training.java.business.dto.LibraryDTO;
import fr.crossthink.training.java.business.facades.LibraryFacade;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

/**
 * Book controller for books view
 */
@Controller
public class BookController {

    private LibraryFacade libraryFacade;


    /**
     * Constructor
     *
     * @param libraryFacade
     */
    public BookController(LibraryFacade libraryFacade) {
        this.libraryFacade = libraryFacade;
    }

    private BookCreateVM bookCreateVM;

    @GetMapping("/book")
    public String homePage(Model model) {
        bookCreateVM = new BookCreateVM();
        List<BookDTO> books = libraryFacade.getAllBooks();

        List<LibraryDTO> libraries = libraryFacade.getAllLibraries();
        model.addAttribute("books", books);
        model.addAttribute("libraries", libraries);
        model.addAttribute("book", bookCreateVM);
        return "book/books";
    }

    @PostMapping("/book/create")
    public String createBook(@Valid @ModelAttribute BookCreateVM bookCreateVM, BindingResult bindingResult) {
        if (bindingResult.getErrorCount() == 0 && bookCreateVM != null) {
            BookDTO bookDTO = new BookDTO(bookCreateVM);
            LibraryDTO libraryDTO = libraryFacade.getLibrary(bookCreateVM.getLibraryId());
            bookDTO.setLibrary(libraryDTO);

            libraryFacade.saveBook(bookDTO);
        }

        return "redirect:/book";
    }

    @PostMapping("/book/delete/{id}")
    public String createBook(@PathVariable Long id) {
        if (id != null) {
            libraryFacade.deleteBook(id);
        }

        return "redirect:/book";
    }
}
