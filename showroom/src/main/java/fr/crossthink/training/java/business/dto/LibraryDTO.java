package fr.crossthink.training.java.business.dto;

import fr.crossthink.training.java.api.vm.LibraryCreateVM;

import java.util.Objects;

public class LibraryDTO {

    private long id;

    private String name;

    private String city;

    public LibraryDTO() {}

    public LibraryDTO(LibraryCreateVM libraryCreateVM) {
        this.name = libraryCreateVM.getName();
        this.city = libraryCreateVM.getCity();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LibraryDTO that = (LibraryDTO) o;
        return name.equals(that.name) &&
                city.equals(that.city);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, city);
    }
}
