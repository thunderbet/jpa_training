package fr.crossthink.training.java.business.services;

import fr.crossthink.training.java.business.repositories.AircraftRepository;
import org.springframework.stereotype.Service;

@Service
public class AircraftService {

    public AircraftRepository aircraftRepository;

    public AircraftService(AircraftRepository aircraftRepository) {
        this.aircraftRepository = aircraftRepository;
    }
}
