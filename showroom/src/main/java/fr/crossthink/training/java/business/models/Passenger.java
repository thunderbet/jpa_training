package fr.crossthink.training.java.business.models;

import javax.persistence.*;

@Entity
@Table(name = "PASSENGER",
        indexes = {
                @Index(
                        name = "IDX_FIRST_NAME",
                        columnList = "FIRST_NAME",
                        unique = false
                ),
                @Index(
                        name = "IDX_LAST_NAME",
                        columnList = "LAST_NAME",
                        unique = false
                )
        })
public class Passenger {

    @Id
    @Column(name="PASSENGER_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "FIRST_NAME", nullable = false)
    private String firstName;

    @Column(name = "LAST_NAME", nullable = false)
    private String lastName;

    @ManyToOne
    @JoinColumn(name = "FLIGHT_ID")
    private Flight flight;

    @Column(name = "REGISTERED", nullable = false)
    private boolean registered;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }
}
