package fr.crossthink.training.java.business.services;

import fr.crossthink.training.java.business.repositories.AirportRepository;
import org.springframework.stereotype.Service;

@Service
public class AirportService {

    private AirportRepository airportRepository;

    public AirportService(AirportRepository airportRepository) {
        this.airportRepository = airportRepository;
    }
}
