package fr.crossthink.training.java;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main class for launching the application.
 * <p>
 * The @SpringBootApplication contains the following :
 *
 * @SpringBootConfiguration
 * @EnableAutoConfiguration
 * @ComponentScan <p>
 * <p>
 * The @SpringBootApplication will detect all the components, entities, repositories, services
 * as long as they are in the same package or the sub packages.
 * <p>
 * So no need for @ComponentScan, @EnableJpaRepositories, etc...
 * The @EnableAutoConfiguration does it for you.
 */
@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}