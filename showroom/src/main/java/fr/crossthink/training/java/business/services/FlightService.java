package fr.crossthink.training.java.business.services;

import fr.crossthink.training.java.business.repositories.FlightRepository;
import org.springframework.stereotype.Service;

@Service
public class FlightService {

    private FlightRepository flightRepository;

    public FlightService(FlightRepository flightRepository) {
        this.flightRepository = flightRepository;
    }
}
