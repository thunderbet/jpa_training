package fr.crossthink.training.java.business.repositories;

import fr.crossthink.training.java.business.models.Airport;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AirportRepository extends JpaRepository<Airport, Long> {

}
