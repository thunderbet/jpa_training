package fr.crossthink.training.java.business.repositories;

import fr.crossthink.training.java.business.models.Aircraft;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AircraftRepository extends JpaRepository<Aircraft, Long> {
}
