package fr.crossthink.training.java.business.models;

import fr.crossthink.training.java.api.vm.LibraryCreateVM;
import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "LIBRARY",
        indexes = {
                @Index(
                        name = "IDX_LIBRARY_NAME",
                        columnList = "NAME",
                        unique = false
                ),
                @Index(
                        name = "IDX_LIBRARY_CITY",
                        columnList = "CITY",
                        unique = false
                ),
        }
)
public class Library {

    @Id
    @Column(name="ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "NAME", nullable = false, unique = true)
    private String name;

    @Column(name = "CITY", nullable = false)
    private String city;

    @OneToMany(mappedBy = "library", fetch = FetchType.LAZY)
    private List<Book> bookList;

    public Library() {}

    public Library(LibraryCreateVM libraryCreateVM) {
        this.name = libraryCreateVM.getName();
        this.city = libraryCreateVM.getCity();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public List<Book> getBookList() {
        return bookList;
    }

    public void setBookList(List<Book> bookList) {
        this.bookList = bookList;
    }
}
