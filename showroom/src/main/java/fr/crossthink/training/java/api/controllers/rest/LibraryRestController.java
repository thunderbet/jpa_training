package fr.crossthink.training.java.api.controllers.rest;

import fr.crossthink.training.java.business.models.Library;
import fr.crossthink.training.java.business.repositories.LibraryRepository;
import fr.crossthink.training.java.exceptions.LibraryNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Rest API for libraries
 */
@RestController
@RequestMapping("/api/libraries")
public class LibraryRestController {

    private LibraryRepository libraryRepository;

    public LibraryRestController(LibraryRepository libraryRepository) {
        this.libraryRepository = libraryRepository;
    }

    @GetMapping("/name/{libraryName}")
    public Library findByName(@PathVariable String libraryName) {
        return libraryRepository.findByName(libraryName);
    }

    @GetMapping("/city/{libraryCity}")
    public List<Library> findByCity(@PathVariable String libraryCity) {
        return libraryRepository.findByCity(libraryCity);
    }

    @GetMapping("/{id}")
    public Library findOne(@PathVariable Long id) {
        return libraryRepository.findById(id)
                .orElseThrow(LibraryNotFoundException::new);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Library create(@RequestBody Library library) {
        return libraryRepository.save(library);
    }


}
