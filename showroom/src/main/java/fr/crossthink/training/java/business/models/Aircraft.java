package fr.crossthink.training.java.business.models;

import javax.persistence.*;

@Entity
@Table(name = "AIRCRAFT",
        indexes = {
                @Index(
                        name = "IDX_BRAND",
                        columnList = "BRAND",
                        unique = false
                ),
                @Index(
                        name = "IDX_MODEL",
                        columnList = "MODEL",
                        unique = false
                )
        })
public class Aircraft {

    @Id
    @Column(name="AIRCRAFT_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "BRAND", nullable = false)
    private String brand;

    @Column(name = "MODEL", nullable = false)
    private String model;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
