package fr.crossthink.training.java.business.facades;

import fr.crossthink.training.java.business.services.AircraftService;
import fr.crossthink.training.java.business.services.AirportService;
import fr.crossthink.training.java.business.services.FlightService;
import fr.crossthink.training.java.business.services.PassengerService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Flying facade to manage all the flight related processes
 */
@Service
@Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
public class FlyingFacade {

    // Services
    private FlightService flightService;
    private PassengerService passengerService;
    private AirportService airportService;
    private AircraftService aircraftService;

    /**
     * Constructor
     *
     * @param flightService
     * @param passengerService
     * @param airportService
     * @param aircraftService
     */
    public FlyingFacade(FlightService flightService,
                        PassengerService passengerService,
                        AirportService airportService,
                        AircraftService aircraftService) {
        this.flightService = flightService;
        this.passengerService = passengerService;
        this.aircraftService = aircraftService;
        this.airportService = airportService;
    }
}
