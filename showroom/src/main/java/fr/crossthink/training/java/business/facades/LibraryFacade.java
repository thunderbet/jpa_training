package fr.crossthink.training.java.business.facades;

import fr.crossthink.training.java.business.dto.BookDTO;
import fr.crossthink.training.java.business.dto.LibraryDTO;
import fr.crossthink.training.java.business.models.Book;
import fr.crossthink.training.java.business.models.Library;
import fr.crossthink.training.java.business.services.BookService;
import fr.crossthink.training.java.business.services.LibraryService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Library facade for managing the library rental services
 */
@Service
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class LibraryFacade {

    // Services
    private BookService bookService;
    private LibraryService libraryService;

    // Utils
    private  ModelMapper modelMapper;

    /**
     * Constructor with parameter injection
     *
     * @param bookService
     * @param libraryService
     */
    public LibraryFacade(BookService bookService, LibraryService libraryService) {
        this.bookService = bookService;
        this.libraryService = libraryService;
        modelMapper = new ModelMapper();
    }

    /**
     * Gets all books
     *
     * @return
     */
    public List<BookDTO> getAllBooks() {
        List<Book> bookList = bookService.getAllBooks();

        return bookList.stream().map(book -> modelMapper.map(book, BookDTO.class)).collect(Collectors.toList());
    }

    /**
     * Gets all libraries
     *
     * @return
     */
    public List<LibraryDTO> getAllLibraries() {
        List<Library> libraryList = libraryService.getAllLibraries();

        return libraryList.stream().map(library -> modelMapper.map(library, LibraryDTO.class)).collect(Collectors.toList());
    }

    /**
     * Gets a library with the given id
     *
     * @param id
     * @return
     */
    public LibraryDTO getLibrary(Long id) {
        Library library = libraryService.getLibrary(id);

        return modelMapper.map(library, LibraryDTO.class);
    }

    /**
     * Saves a book in one call
     *
     * @param bookDTO
     * @return
     */
    public BookDTO saveBook(BookDTO bookDTO) {
        Book book = bookService.saveBook(modelMapper.map(bookDTO, Book.class));

        // throw new RuntimeException();

        return modelMapper.map(book, BookDTO.class);
    }

    /**
     * Saves a book in one call
     *
     * @param bookDTO
     * @return
     */
    public BookDTO saveBookAndLibrary(BookDTO bookDTO) {
        Book bookToSave = modelMapper.map(bookDTO, Book.class);
        Library libraryToSave = bookToSave.getLibrary();

        Library savedLibrary = libraryService.saveLibrary(libraryToSave);
        bookToSave.setLibrary(savedLibrary);

        // Unexpected exception
        String strNull = null;
        strNull.substring(0);

        Book savedBook = bookService.saveBook(bookToSave);

        return modelMapper.map(savedBook, BookDTO.class);
    }

    /**
     * Deletes a book with the given id
     *
     * @param id
     */
    public void deleteBook(Long id) {
        bookService.deleteBook(id);
    }
}
