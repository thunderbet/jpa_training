package fr.crossthink.training.java.business.dto;

import fr.crossthink.training.java.business.models.Aircraft;
import fr.crossthink.training.java.business.models.Airport;
import fr.crossthink.training.java.business.models.Passenger;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

public class FlightDTO {

    private long id;

    private String flightNumber;

    private Airport departureAirport;

    private Airport arrivalAirport;

    private LocalDateTime arrivalTime;

    private LocalDateTime departureTime;

    private Aircraft aircraft;

    private List<Passenger> passengerList;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public Airport getDepartureAirport() {
        return departureAirport;
    }

    public void setDepartureAirport(Airport departureAirport) {
        this.departureAirport = departureAirport;
    }

    public Airport getArrivalAirport() {
        return arrivalAirport;
    }

    public void setArrivalAirport(Airport arrivalAirport) {
        this.arrivalAirport = arrivalAirport;
    }

    public LocalDateTime getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(LocalDateTime arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public LocalDateTime getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(LocalDateTime departureTime) {
        this.departureTime = departureTime;
    }

    public Aircraft getAircraft() {
        return aircraft;
    }

    public void setAircraft(Aircraft aircraft) {
        this.aircraft = aircraft;
    }

    public List<Passenger> getPassengerList() {
        return passengerList;
    }

    public void setPassengerList(List<Passenger> passengerList) {
        this.passengerList = passengerList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FlightDTO flightDTO = (FlightDTO) o;
        return flightNumber.equals(flightDTO.flightNumber) &&
                departureAirport.equals(flightDTO.departureAirport) &&
                arrivalAirport.equals(flightDTO.arrivalAirport) &&
                arrivalTime.equals(flightDTO.arrivalTime) &&
                departureTime.equals(flightDTO.departureTime) &&
                Objects.equals(aircraft, flightDTO.aircraft);
    }

    @Override
    public int hashCode() {
        return Objects.hash(flightNumber, departureAirport, arrivalAirport, arrivalTime, departureTime, aircraft);
    }
}
