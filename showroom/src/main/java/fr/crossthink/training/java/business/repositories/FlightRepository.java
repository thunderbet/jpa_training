package fr.crossthink.training.java.business.repositories;

import fr.crossthink.training.java.business.models.Flight;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FlightRepository extends JpaRepository<Flight, Long> {
}
