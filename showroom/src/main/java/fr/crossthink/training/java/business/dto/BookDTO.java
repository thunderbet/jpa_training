package fr.crossthink.training.java.business.dto;

import fr.crossthink.training.java.api.vm.BookCreateVM;

import java.util.Objects;

public class BookDTO {

    private long id;

    private String title;

    private String author;

    private LibraryDTO library;

    public BookDTO() {}

    public BookDTO(BookCreateVM bookCreateVM) {
        this.title = bookCreateVM.getTitle();
        this.author = bookCreateVM.getAuthor();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public LibraryDTO getLibrary() {
        return library;
    }

    public void setLibrary(LibraryDTO library) {
        this.library = library;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BookDTO bookDTO = (BookDTO) o;
        return title.equals(bookDTO.title) &&
                author.equals(bookDTO.author);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, author);
    }
}
