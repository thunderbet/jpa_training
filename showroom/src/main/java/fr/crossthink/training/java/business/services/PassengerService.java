package fr.crossthink.training.java.business.services;

import fr.crossthink.training.java.business.repositories.PassengerRepository;
import org.springframework.stereotype.Service;

@Service
public class PassengerService {

    private PassengerRepository passengerRepository;

    public PassengerService(PassengerRepository passengerRepository) {
        this.passengerRepository = passengerRepository;
    }
}
