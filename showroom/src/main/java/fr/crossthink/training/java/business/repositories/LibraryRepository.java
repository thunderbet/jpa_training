package fr.crossthink.training.java.business.repositories;

import fr.crossthink.training.java.business.models.Library;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LibraryRepository extends JpaRepository<Library, Long> {
    Library findByName(String name);
    List<Library> findByCity(String city);

}
