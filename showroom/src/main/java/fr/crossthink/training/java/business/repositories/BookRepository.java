package fr.crossthink.training.java.business.repositories;

import fr.crossthink.training.java.business.models.Book;
import fr.crossthink.training.java.business.models.Library;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.List;

public interface BookRepository extends JpaRepository<Book, Long> {
    // JPA
    List<Book> findByTitle(String title);
    Book findByTitleAndAuthor(String title, String author);
    List<Book> findByLibrary(Library library);
    List<Book> findByAuthor(String authos);

    // JPQL examples
    @Query(name = "findByLibraryName", value = "SELECT book From Book book WHERE book.library.name = ?1")
    List<Book> findByLibraryName(String name);

    @Query(name = "findByLibraryNameAndCity", value = "SELECT book From Book book WHERE book.library.name = ?1 and book.library.city = ?2")
    List<Book> findByLibraryNameAndCity(String name, String city);
}
