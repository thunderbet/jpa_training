package fr.crossthink.training.java.business.repositories;

import fr.crossthink.training.java.business.models.Passenger;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PassengerRepository extends JpaRepository<Passenger, Long> {
}
