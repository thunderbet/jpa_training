package fr.crossthink.training.java.api.controllers.rest;

import fr.crossthink.training.java.business.dto.BookDTO;
import fr.crossthink.training.java.business.facades.LibraryFacade;
import fr.crossthink.training.java.business.models.Book;
import fr.crossthink.training.java.business.repositories.BookRepository;
import fr.crossthink.training.java.exceptions.BookIdMismatchException;
import fr.crossthink.training.java.exceptions.BookNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Rest API controller for books
 */
@RestController
@RequestMapping("/api/books")
public class BookRestController {

    private BookRepository bookRepository;
    private LibraryFacade libraryFacade;

    public BookRestController(BookRepository bookRepository, LibraryFacade libraryFacade) {
        this.bookRepository = bookRepository;
        this.libraryFacade = libraryFacade;
    }

    @GetMapping
    public Iterable findAll() {
        return bookRepository.findAll();
    }

    @GetMapping("/title/{bookTitle}")
    public List findByTitle(@PathVariable String bookTitle) {
        return bookRepository.findByTitle(bookTitle);
    }

    @GetMapping("/library/{libraryName}/{libraryCity}")
    public List findByLibrary(@PathVariable String libraryName, @PathVariable String libraryCity) {
        return bookRepository.findByLibraryNameAndCity(libraryName, libraryCity);
    }

    @GetMapping("/{id}")
    public Book findOne(@PathVariable Long id) {
        return bookRepository.findById(id)
                .orElseThrow(BookNotFoundException::new);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Book create(@RequestBody Book book) {
        return bookRepository.save(book);
    }

    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public BookDTO createBookAndLibrary(@RequestBody BookDTO bookDTO) {
        return libraryFacade.saveBookAndLibrary(bookDTO);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        bookRepository.findById(id)
                .orElseThrow(BookNotFoundException::new);
        bookRepository.deleteById(id);
    }

    @PutMapping("/{id}")
    public Book updateBook(@RequestBody Book book, @PathVariable Long id) {
        if (book.getId() != id) {
            throw new BookIdMismatchException();
        }
        bookRepository.findById(id)
                .orElseThrow(BookNotFoundException::new);
        return bookRepository.save(book);
    }
}