package fr.crossthink.training.java.business.services;

import fr.crossthink.training.java.business.models.Book;
import fr.crossthink.training.java.business.repositories.BookRepository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Book service
 */
@Service
public class BookService {

    private BookRepository bookRepository;

    /**
     * Constructor with parameter injection
     *
     * @param bookRepository
     */
    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    /**
     * Gets the book with the given id
     *
     * @param id
     * @return
     */
    public Book getBookById(Long id) {
        return bookRepository.getOne(id);
    }

    public boolean bookExists(Long id) {
        return bookRepository.existsById(id);
    }

    /**
     * Gets all the books
     *
     * @return
     */
    public List<Book> getAllBooks() {
        return bookRepository.findAll();
    }

    public List<Book> findByAuthor(String author) {
        return bookRepository.findByAuthor(author);
    }

    public Book findByTitleAndAuthor(String title, String author) {
        return bookRepository.findByTitleAndAuthor(title, author);
    }
    /**
     * Saves a book
     *
     * @param book
     * @return
     */
    public Book saveBook(Book book) {
        return bookRepository.save(book);
    }

    /**
     * Deletes a book with the given id
     *
     * @param id
     */
    public void deleteBook(Long id) {
        bookRepository.deleteById(id);
    }

    public void deleteBook(Book book) {
        bookRepository.delete(book);
    }

    /**
     * Deletes all books, for test clean up
     */
    public void deleteAll() { bookRepository.deleteAll(); }
}
