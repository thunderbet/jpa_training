package fr.crossthink.training.java.api.controllers.view;

import fr.crossthink.training.java.api.vm.LibraryCreateVM;
import fr.crossthink.training.java.business.models.Library;
import fr.crossthink.training.java.business.repositories.LibraryRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

/**
 * Library controller for libraries view
 */
@Controller
public class LibraryController {

    private LibraryRepository libraryRepository;

    public LibraryController(LibraryRepository libraryRepository) {
        this.libraryRepository = libraryRepository;
    }

    private LibraryCreateVM libraryCreateVM;

    @GetMapping("/library")
    public String homePage(Model model) {
        libraryCreateVM = new LibraryCreateVM();
        List<Library> libraries = libraryRepository.findAll();
        model.addAttribute("libraries", libraries);
        model.addAttribute("library", libraryCreateVM);
        return "library/libraries";
    }

    @PostMapping("/library/create")
    public String createLibrary(@Valid @ModelAttribute LibraryCreateVM libraryCreateVM, BindingResult bindingResult) {
        if (libraryCreateVM != null) {
            Library library = new Library(libraryCreateVM);
            libraryRepository.save(library);
        }

        return "redirect:/library";
    }

    @PostMapping("/library/delete/{id}")
    public String createBook(@PathVariable Long id) {
        if (id != null) {
            libraryRepository.deleteById(id);
        }

        return "redirect:/library";
    }
}
