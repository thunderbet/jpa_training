package fr.crossthink.training.java.business.models;

import javax.persistence.*;

@Entity
@Table(name = "AIRPORT",
        indexes = {
                @Index(
                        name = "IDX_CITY",
                        columnList = "CITY",
                        unique = false
                ),
                @Index(
                        name = "IDX_CITY_CODE",
                        columnList = "CITY_CODE",
                        unique = false
                )
        })
public class Airport {

    @Id
    @Column(name="AIRPORT_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "CITY", nullable = false)
    private String city;

    @Column(name = "CITY_CODE", nullable = false)
    private String cityCode;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }
}
