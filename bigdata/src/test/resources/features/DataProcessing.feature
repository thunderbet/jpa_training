Feature: Testing the data processing pipeline

  Scenario: Run the pipeline
    Given the following lines :
      | date     | number | horse      | driver        | sex | rating | age | position | distance | participants | discipline | race   | reunion   |
      |2019-12-15| 1      | FLORE RITE | Y LEBOURGEOIS | F   | 18     | 4   | 9        | 2100     | 12           | ATTELE     | ANCTGP | VINCENNES |
    When the pipeline is run
    Then the output is written