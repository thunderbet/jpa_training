package fr.crossthink.training.java.test.rwfunctions;

import fr.crossthink.training.java.HorseLine;
import fr.crossthink.training.java.exceptions.WrongDataFormatException;
import fr.crossthink.training.java.rwfunctions.HorseReader;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class HorseReaderTest {

    private static final String HEADER_LINE = "DATE;NUMERO;CHEVAL;DRIVER;SEXE;COTE;AGE;POSITION;DISTANCE;PARTANTS;DISCIPLINE;COURSE;REUNION";
    private static final String DATA_LINE = "2019-12-15;1;FLORE RITE;Y LEBOURGEOIS;F;18;4;9;2100;12;ATTELE;ANCTGP;VINCENNES";
    private static final String WRONG_FORMAT_DATA_LINE = "2019-12-15;1;FLORE RITE;Y LEBOURGEOIS;F;18;4;9";

    HorseReader horseReader;

    @Before
    public void setup() {
        horseReader = new HorseReader();
    }

    @Test
    public void shouldReadHorseFromLine() {
        HorseLine horseLine = horseReader.apply(DATA_LINE);

        assertThat(horseLine).isNotNull();
    }

    @Test
    public void shouldNotReadHeaderFromLine() {
        Exception exception = assertThrows(NumberFormatException.class, () -> {
            horseReader.apply(HEADER_LINE);
        });

        assertThat(exception).isNotNull();
        assertThat(exception).isOfAnyClassIn(NumberFormatException.class);
    }

    @Test
    public void shouldNotReadWrongFormatLine() {
        Exception exception = assertThrows(WrongDataFormatException.class, () -> {
            horseReader.apply(WRONG_FORMAT_DATA_LINE);
        });

        assertThat(exception).isNotNull();
        assertThat(exception).isOfAnyClassIn(WrongDataFormatException.class);
        assertThat(exception.getMessage()).isEqualTo("Wrong data format in line : " + WRONG_FORMAT_DATA_LINE);
    }
}
