package fr.crossthink.training.java.test.it;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"classpath:features"},
        glue = {"classpath:fr.crossthink.training.java.test.it.glue"},
        tags = {"not @ignore"},
        strict = true)
public class CukeRunnerIT {
}
