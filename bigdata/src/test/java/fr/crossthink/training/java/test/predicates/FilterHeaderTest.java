package fr.crossthink.training.java.test.predicates;

import fr.crossthink.training.java.predicates.FilterHeader;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class FilterHeaderTest {

    private static final String HEADER_LINE = "DATE;NUMERO;CHEVAL;DRIVER;SEXE;COTE;AGE;POSITION;DISTANCE;PARTANTS;DISCIPLINE;COURSE;REUNION";
    private static final String DATA_LINE = "2019-12-15;1;FLORE RITE;Y LEBOURGEOIS;F;18;4;9;2100;12;ATTELE;ANCTGP;VINCENNES";

    FilterHeader filterHeader;

    @Before
    public void setup() {
        filterHeader = new FilterHeader();
    }

    @Test
    public void shouldNotAcceptHeader() {
        boolean accepted = filterHeader.test(HEADER_LINE);

        assertThat(accepted).isFalse();
    }

    @Test
    public void shouldAcceptLine() {
        boolean accepted = filterHeader.test(DATA_LINE);

        assertThat(accepted).isTrue();
    }
}
