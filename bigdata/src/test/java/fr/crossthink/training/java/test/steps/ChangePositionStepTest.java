package fr.crossthink.training.java.test.steps;

import fr.crossthink.training.java.steps.ChangePositionStep;
import org.apache.crunch.Emitter;
import org.apache.crunch.Pair;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ChangePositionStepTest {

    private static final Long KEY = 50L;
    private static final String VALUE = "Value";

    ChangePositionStep changePositionStep;
    MockEmitter<Pair<String, Long>> emitter;

    @Before
    public void setup() {
        changePositionStep = new ChangePositionStep();
        emitter = new MockEmitter();
    }

    @Test
    public void shouldChangeKevValuePositions() {
        Pair<Long, String> keyValuePair = Pair.of(KEY, VALUE);

        changePositionStep.process(keyValuePair, emitter);

        assertThat(emitter.getEmitted()).isNotNull();
        assertThat(emitter.getEmitted()).isEqualTo(Pair.of(VALUE, KEY));
    }
}

/**
 * Mock emitter
 *
 * @param <T>
 */
class MockEmitter<T> implements Emitter<T> {
    T emitted;

    public void emit(T stringLongPair) {
        emitted = stringLongPair;
    }

    @Override
    public void flush() {
    }

    public T getEmitted() {
        return emitted;
    }
}
