package fr.crossthink.training.java.test.steps;

import fr.crossthink.training.java.HorseLine;
import fr.crossthink.training.java.keys.DriverHorseKey;
import fr.crossthink.training.java.steps.DriverHorseKeyStep;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class DriverHorseKeyStepTest {

    private static final String HORSE = "Horse";
    private static final String DRIVER = "Driver";

    DriverHorseKeyStep driverHorseKeyStep;

    @Before
    public void setup() {
        driverHorseKeyStep = new DriverHorseKeyStep();
    }

    @Test
    public void shouldMapToDriverHorseKey() {
        HorseLine horse = HorseLine.newBuilder()
                .setHorse(HORSE)
                .setDriver(DRIVER)
                .build();

        DriverHorseKey driverHorseKey = driverHorseKeyStep.map(horse);

        assertThat(driverHorseKey).isNotNull();
        assertThat(driverHorseKey.getHorse()).isEqualTo(HORSE);
        assertThat(driverHorseKey.getDriver()).isEqualTo(DRIVER);
    }
}
