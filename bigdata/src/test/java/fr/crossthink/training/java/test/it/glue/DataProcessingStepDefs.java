package fr.crossthink.training.java.test.it.glue;

import fr.crossthink.training.java.HorseLine;
import fr.crossthink.training.java.processing.DataProcessing;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.commons.io.FileUtils;
import org.assertj.core.util.Arrays;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class DataProcessingStepDefs {

    DataProcessing dataProcessing;

    List<HorseLine> lines;

    File dataFile;

    File avroFile;

    File distinctHorses;

    File driverHorses;

    @Before
    public void setUp() throws IOException {
        dataProcessing = new DataProcessing();
        dataFile = File.createTempFile("horseLines", ".csv");
        avroFile = File.createTempFile("horses", ".avro");
        distinctHorses = new File(DataProcessing.DISTINCT_HORSES_TXT);
        driverHorses = new File(DataProcessing.DRIVER_HORSES_TXT);
    }

    @After
    public void tearDown() throws IOException {
        FileUtils.deleteQuietly(dataFile);
        FileUtils.deleteQuietly(avroFile);
        FileUtils.deleteDirectory(distinctHorses);
        FileUtils.deleteDirectory(driverHorses);
    }

    @Given("the following lines :")
    public void theFollowingConfiguration(List<HorseLine> lines) throws IOException {
        this.lines = lines;
        FileWriter fileWriter = new FileWriter(dataFile);
        fileWriter.write("DATE;NUMERO;CHEVAL;DRIVER;SEXE;COTE;AGE;POSITION;DISTANCE;PARTANTS;DISCIPLINE;COURSE;REUNION");
        fileWriter.write('\n');
        fileWriter.write(horseLineToCsv(lines.get(0)));
        fileWriter.close();
    }

    private String horseLineToCsv(HorseLine horseLine) {
        return horseLine.getDate() + ";" + horseLine.getNumber() + ";" + horseLine.getHorse() + ";" + horseLine.getDriver() + ";" + horseLine.getSex()
                + ";" + horseLine.getRating() + ";" + horseLine.getAge() + ";" + horseLine.getPosition() + ";" + horseLine.getDistance() + ";" + horseLine.getParticipants()
                + ";" + horseLine.getDiscipline() + ";" + horseLine.getRace() + ";" + horseLine.getReunion();
    }

    @When("the pipeline is run")
    public void theProcessRuns() throws IOException {
        dataProcessing.run(Arrays.array(dataFile.getAbsolutePath(), avroFile.getAbsolutePath(), "true"));
    }

    @Then("the output is written")
    public void theTheOutputFoldersAreCreated() {
        assertThat(distinctHorses).exists();
        assertThat(distinctHorses).isDirectory();
        assertThat(driverHorses).exists();
        assertThat(driverHorses).isDirectory();
    }
}
