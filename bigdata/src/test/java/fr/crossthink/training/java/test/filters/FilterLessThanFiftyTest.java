package fr.crossthink.training.java.test.filters;

import fr.crossthink.training.java.filters.FilterLessThanFifty;
import fr.crossthink.training.java.keys.DriverHorseKey;
import org.apache.crunch.Pair;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class FilterLessThanFiftyTest {

    private static final String HORSE = "Horse";
    private static final String DRIVER = "Driver";
    private static final Long OVER_FIFTY = 52L;
    private static final Long FIFTY = 50L;
    private static final Long UNDER_FIFTY = 49L;

    FilterLessThanFifty filterLessThanFifty;

    @Before
    public void setup() {
        filterLessThanFifty = new FilterLessThanFifty();
    }

    @Test
    public void shouldAcceptOverFifty() {
        Pair<Long, DriverHorseKey> driverHorseKeyLongPair = Pair.of(OVER_FIFTY, new DriverHorseKey(DRIVER, HORSE));

        boolean filtered = filterLessThanFifty.accept(driverHorseKeyLongPair);

        assertThat(filtered).isTrue();
    }

    @Test
    public void shouldAcceptFifty() {
        Pair<Long, DriverHorseKey> driverHorseKeyLongPair = Pair.of(FIFTY, new DriverHorseKey(DRIVER, HORSE));

        boolean filtered = filterLessThanFifty.accept(driverHorseKeyLongPair);

        assertThat(filtered).isTrue();
    }

    @Test
    public void shouldNotAcceptUnderFifty() {
        Pair<Long, DriverHorseKey> driverHorseKeyLongPair = Pair.of(UNDER_FIFTY, new DriverHorseKey(DRIVER, HORSE));

        boolean filtered = filterLessThanFifty.accept(driverHorseKeyLongPair);

        assertThat(filtered).isFalse();
    }
}
