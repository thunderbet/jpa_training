package fr.crossthink.training.java.test.steps;

import fr.crossthink.training.java.HorseLine;
import fr.crossthink.training.java.steps.HorseNameStep;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class HorseNameStepTest {

    private static final String HORSE = "Horse";

    HorseNameStep horseNameStep;

    @Before
    public void setup() {
        horseNameStep = new HorseNameStep();
    }

    @Test
    public void shoudMapToHorseName() {
        HorseLine horse = HorseLine.newBuilder()
                .setHorse(HORSE)
                .build();

        String horseName = horseNameStep.map(horse);

        assertThat(horseName).isEqualTo(HORSE);
    }
}
