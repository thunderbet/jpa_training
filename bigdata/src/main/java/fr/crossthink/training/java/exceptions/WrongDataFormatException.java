package fr.crossthink.training.java.exceptions;

/**
 * Exception for wrong data format in CSV
 */
public class WrongDataFormatException extends RuntimeException {

    private static final String ERROR_MESSAGE = "Wrong data format in line : ";

    public WrongDataFormatException(String line) {
        super(ERROR_MESSAGE + line);
    }
}
