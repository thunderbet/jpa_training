package fr.crossthink.training.java;

import fr.crossthink.training.java.processing.DataProcessing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

/**
 * Main class for launching the application.
 * <p>
 * The @SpringBootApplication contains the following :
 *
 * @SpringBootConfiguration
 * @EnableAutoConfiguration
 * @ComponentScan <p>
 * <p>
 * The @SpringBootApplication will detect all the components, entities, repositories, services
 * as long as they are in the same package or the sub packages.
 * <p>
 * So no need for @ComponentScan, @EnableJpaRepositories, etc...
 * The @EnableAutoConfiguration does it for you.
 */
@SpringBootApplication
public class Application {

    @Autowired
    DataProcessing dataProcessing;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public CommandLineRunner runPreprocessRunner() {
        return args -> dataProcessing.run(args);
    }
}