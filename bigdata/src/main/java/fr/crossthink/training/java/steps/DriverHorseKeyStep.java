package fr.crossthink.training.java.steps;

import fr.crossthink.training.java.HorseLine;
import fr.crossthink.training.java.keys.DriverHorseKey;
import org.apache.crunch.MapFn;

/**
 * Groups the horses by driver
 */
public class DriverHorseKeyStep extends MapFn<HorseLine, DriverHorseKey> {

    @Override
    public DriverHorseKey map(HorseLine horse) {
        increment("horses", "read");
        return new DriverHorseKey(horse.getDriver(), horse.getHorse());
    }
}
