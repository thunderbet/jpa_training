package fr.crossthink.training.java.rwfunctions;

import fr.crossthink.training.java.HorseLine;
import org.apache.avro.file.DataFileWriter;

import java.io.IOException;
import java.util.function.Consumer;

/**
 * Writes horses to avro file
 */
public class HorseToAvroWriter implements Consumer<HorseLine> {

    private DataFileWriter<HorseLine> writer;

    public HorseToAvroWriter(DataFileWriter<HorseLine> writer) {
        this.writer = writer;
    }

    @Override
    public void accept(HorseLine horse) {
        try {
            writer.append(horse);
        } catch (IOException ie) {
            throw new RuntimeException(ie);
        }
    }
}
