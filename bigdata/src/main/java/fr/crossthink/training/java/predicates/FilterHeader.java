package fr.crossthink.training.java.predicates;

import java.util.function.Predicate;

/**
 * Checks that the CSV line is not a header
 */
public class FilterHeader implements Predicate<String> {

    @Override
    public boolean test(String line) {
        return !line.startsWith("DATE");
    }
}
