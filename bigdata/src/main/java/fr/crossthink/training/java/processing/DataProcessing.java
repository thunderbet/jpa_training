package fr.crossthink.training.java.processing;

import fr.crossthink.training.java.HorseLine;
import fr.crossthink.training.java.filters.FilterLessThanFifty;
import fr.crossthink.training.java.keys.DriverHorseKey;
import fr.crossthink.training.java.predicates.FilterHeader;
import fr.crossthink.training.java.rwfunctions.HorseReader;
import fr.crossthink.training.java.rwfunctions.HorseToAvroWriter;
import fr.crossthink.training.java.steps.ChangePositionStep;
import fr.crossthink.training.java.steps.DriverHorseKeyStep;
import fr.crossthink.training.java.steps.HorseNameStep;
import org.apache.avro.file.CodecFactory;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.specific.SpecificDatumWriter;
import org.apache.crunch.*;
import org.apache.crunch.impl.mem.MemPipeline;
import org.apache.crunch.io.From;
import org.apache.crunch.io.To;
import org.apache.crunch.lib.Distinct;
import org.apache.crunch.lib.Sort;
import org.apache.crunch.types.avro.Avros;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.FileUrlResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.charset.StandardCharsets;

@Component
public class DataProcessing extends Configured {

    private static final Logger LOGGER = LoggerFactory.getLogger(DataProcessing.class);

    public static final String DRIVER_HORSES_TXT = "C:\\git\\horseNames.txt";
    public static final String DISTINCT_HORSES_TXT = "C:\\git\\distinctHorses.txt";

    public void run(String[] args) throws IOException {
        String inputPath = args[0];
        String avroPath = args[1];
        boolean convertToAvro = args.length > 2 ? Boolean.parseBoolean(args[2]) : false;

        Resource fileResource = new FileUrlResource(inputPath);
        File avroFileName = new File(avroPath);

        if (convertToAvro) {
            convertCsvToAvro(fileResource, avroFileName);
        }

        processPipeline(avroFileName.getAbsolutePath());
    }

    /**
     * Runs the main pipeline
     *
     * @param avroPath
     */
    private void processPipeline(String avroPath) {
//        Supplier<Pipeline> hadoopPipelineSupplier = () -> new MRPipeline(DataProcessing.class, getConf());
//        Pipeline pipeline = hadoopPipelineSupplier.get();

        Pipeline pipeline = MemPipeline.getInstance();
        Source<HorseLine> pipelineSupplier = From.avroFile(avroPath, Avros.records(HorseLine.class));

        // Read the data
        PCollection<HorseLine> horseCollection = pipeline.read(pipelineSupplier);

        // Group by name
        PCollection<String> horseNameCollection = horseCollection.parallelDo("HorseNameStep", new HorseNameStep(), Avros.strings());
        // Distinct names
        PCollection<String> distinctHorseNames = Distinct.distinct(horseNameCollection);

        // Group by driver
        PCollection<DriverHorseKey> driverHorseCollection = horseCollection.parallelDo("DriverStep", new DriverHorseKeyStep(), Avros.records(DriverHorseKey.class));
        // Count de driver/horse pairs
        PTable<DriverHorseKey, Long> driverHorseCount = driverHorseCollection.count();
        // Change the key and value positions
        PTable<Long, DriverHorseKey> driverHorsesTable = driverHorseCount
                .parallelDo(new ChangePositionStep<>(), Avros.tableOf(Avros.longs(), Avros.records(DriverHorseKey.class)))
                .filter(new FilterLessThanFifty());
        // Sort by key (number or pairs)
        PTable<Long, DriverHorseKey> sortedDriverHorseCollection = Sort.sort(driverHorsesTable, Sort.Order.DESCENDING);

        // Writing the results
        // Distinct horse names
        Target distinctHorsesTarget = To.textFile(new Path(DISTINCT_HORSES_TXT));
        distinctHorseNames.write(distinctHorsesTarget);

        // Grouped driver/horses pairs
        Target driverHorsesTarget = To.textFile(new Path(DRIVER_HORSES_TXT));
        sortedDriverHorseCollection.write(driverHorsesTarget);

        // Finish processing
        PipelineResult results = pipeline.done();

        LOGGER.info("Number of horses read : " + results.getStageResults().get(0).getCounterDisplayName("horses", "read"));
    }

    /**
     * Converts the CSV file into an AVRO file
     *
     * @param fileResource
     * @param avroFileName
     * @throws IOException
     */
    private void convertCsvToAvro(Resource fileResource, File avroFileName) throws IOException {
        try (FileOutputStream dataOutputStream = new FileOutputStream(avroFileName);
             DataFileWriter<HorseLine> dataFileWriter =
                     new DataFileWriter<>(new SpecificDatumWriter<>(HorseLine.class))) {

            dataFileWriter.setCodec(CodecFactory.snappyCodec());
            dataFileWriter.create(HorseLine.getClassSchema(), dataOutputStream);

            try (BufferedReader bufferedReader =
                         new BufferedReader(new InputStreamReader(fileResource.getInputStream(), StandardCharsets.UTF_8))) {

                bufferedReader
                        .lines()
                        .filter(new FilterHeader())
                        .map(new HorseReader())
                        .forEach(new HorseToAvroWriter(dataFileWriter));

                LOGGER.info("Conversion to AVRO completed");
            }
        }
    }
}
