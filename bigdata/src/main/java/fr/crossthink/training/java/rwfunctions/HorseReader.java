package fr.crossthink.training.java.rwfunctions;

import fr.crossthink.training.java.HorseLine;
import fr.crossthink.training.java.exceptions.WrongDataFormatException;

import java.util.function.Function;

/**
 * Converts a CSV line into a Horse
 */
public class HorseReader implements Function<String, HorseLine> {

    @Override
    public HorseLine apply(String line) {
        HorseLine horse = null;
        String[] lineTab = line.split(";");

        try {
            if (lineTab.length == 13) {
                horse = HorseLine.newBuilder()
                        .setDate(lineTab[0])
                        .setNumber(Integer.parseInt(lineTab[1]))
                        .setHorse(lineTab[2])
                        .setDriver(lineTab[3])
                        .setSex(lineTab[4])
                        .setRating(Double.parseDouble(lineTab[5]))
                        .setAge(Integer.parseInt(lineTab[6]))
                        .setPosition(Integer.parseInt(lineTab[7]))
                        .setDistance(Integer.parseInt(lineTab[8]))
                        .setParticipants(Integer.parseInt(lineTab[9]))
                        .setDiscipline(lineTab[10])
                        .setRace(lineTab[11])
                        .setReunion(lineTab[12])
                        .build();
            } else {
                throw new WrongDataFormatException(line);
            }
        } catch (NumberFormatException nex) {
            throw nex;
        }

        return horse;
    }
}
