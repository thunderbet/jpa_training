package fr.crossthink.training.java.filters;

import fr.crossthink.training.java.keys.DriverHorseKey;
import org.apache.crunch.FilterFn;
import org.apache.crunch.Pair;

/**
 * Filters the driver/horse pair that have less than 50 appearances
 */
public class FilterLessThanFifty extends FilterFn<Pair<Long, DriverHorseKey>> {
    private final int NUMBER_APPREARENCES = 50;

    @Override
    public boolean accept(Pair<Long, DriverHorseKey> driverHorseKeyLongPair) {
        return driverHorseKeyLongPair.first() >= NUMBER_APPREARENCES;
    }
}
