package fr.crossthink.training.java.steps;

import org.apache.crunch.DoFn;
import org.apache.crunch.Emitter;
import org.apache.crunch.Pair;

/**
 * Change the position of the key and value
 */
public class ChangePositionStep<K, V> extends DoFn<Pair<K, V>, Pair<V, K>> {

    @Override
    public void process(Pair<K, V> stringLongPair, Emitter<Pair<V, K>> emitter) {
        emitter.emit(Pair.of(stringLongPair.second(), stringLongPair.first()));
    }
}
