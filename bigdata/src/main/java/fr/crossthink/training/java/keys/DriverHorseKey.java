package fr.crossthink.training.java.keys;

import java.io.Serializable;
import java.util.Objects;

public class DriverHorseKey implements Serializable {

    static final long serialVersionUID = 1L;

    private String driver;

    private String horse;

    public DriverHorseKey(String driver, String horse) {
        this.driver = driver;
        this.horse = horse;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getHorse() {
        return horse;
    }

    public void setHorse(String horse) {
        this.horse = horse;
    }

    @Override
    public String toString() {
        return driver + " : " + horse;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DriverHorseKey that = (DriverHorseKey) o;
        return driver.equals(that.driver) &&
                horse.equals(that.horse);
    }

    @Override
    public int hashCode() {
        return Objects.hash(driver, horse);
    }
}
