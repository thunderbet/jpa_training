package fr.crossthink.training.java.steps;

import fr.crossthink.training.java.HorseLine;
import org.apache.crunch.MapFn;

/**
 * Groups the horses by horse name
 */
public class HorseNameStep extends MapFn<HorseLine, String> {
    @Override
    public String map(HorseLine horse) {
        return horse.getHorse();
    }
}
